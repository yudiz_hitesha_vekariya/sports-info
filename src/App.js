
import './App.css';
import CardView from './components/CardView';

function App() {
  return (
    <div className="App">
     <div>
      <CardView />
     </div>
    </div>
  );
}

export default App;
