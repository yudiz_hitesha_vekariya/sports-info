import React from "react";
import sports from "../sports.json";

const CardView = () => {
  return (
    <div className="const">
      {/* <h1>Hitesha Vekariya</h1> */}
      {sports.data.map((sportDetail, index) => {
        return (
          <div key={index} className="main">
            <div>
              <img src={sportDetail.sImage} alt=""></img>
            </div>
            <div className="dis">
              <div className="content"> 
                <h1 className="title">{sportDetail.sTitle}</h1>
                <p className="des">{sportDetail.sDescription}...</p>
              </div>
              <div className="h4h">
                <div>
                  <p className="para">
                    <span className="span">
                      {sportDetail.iId.sFirstName} {sportDetail.iId.sLastName}
                    </span>{" "}
                    a year ago
                  </p>
                </div>
                <div className="count">
                  <h4>
                    <img
                      className="icon"
                      src="https://www.sports.info/comment-icon.7aef209a3b2086028430.svg"
                      alt=""
                    ></img>
                    {sportDetail.nCommentsCount}
                  </h4>
                  <h4>
                    <img
                      className="icons"
                      src="https://www.sports.info/view-icon.b16661e96527947b18f1.svg"
                      alt=""
                    ></img>
                    {sportDetail.nViewCounts}
                  </h4>
                </div>
              </div>
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default CardView;
